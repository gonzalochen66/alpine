from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse as FileResponse
from fastapi import File,UploadFile
from typing import Optional
import os

app = FastAPI()

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
async def get_index():
    return {"message": "Hello, FastAPI!"}

@app.get("/html")
async def get_html():
    return FileResponse("static/index.html")

@app.get("/record",description="this is my first route",deprecated=True)
async def record():
    records = os.listdir("static/folders")
    values = []
    for idx,record in enumerate(records):
        values.append({
            "id":str(idx),
            "name":record.split(".")[0],
            "url":record,
            "extention":record.split(".")[1]
        })

    return {
        "files":values
            
    }

@app.post("/upload/")
async def create_upload_file(
    file: UploadFile = File(..., description="A file read as UploadFile")
):
    print('############',file)
    file_path = os.path.join("static/folders", file.filename)
    with open(file_path, "wb") as f:
        f.write(await file.read())

    

    return {"filename": file.filename}
